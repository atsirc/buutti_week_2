Koskien extra-tehtävää:

Siihen kuuluu 3 tiedostoa:
    ex_pankki.js :      pankkitilin tiedot, inquirer, init
    logger.js    :      fs, eli kirjoittaa tiedostoon
    cron.js      :      lokien ylläpito, eli cron

Käynnistys: Pankkitilin käsittely käynnistetään ex_pankki.js:in kautta. 'node .\ex_pankki.js'

Lokien ylläpito: cron.js pitää käynnistää omassa terminaalissa niin että arkivointi sujuu. Se toimii nyt 'node .\cron.js'-komennon kautta. (Fixumpaa olisi tottakai esim tehdä sille oma scripti package.json:iin. Mutta jos tämän tekisi niin varmaan kannattaisi myös tehdä oma kansio projektille etc :). Eli tehtävän mittakaava vain paisuis.
En ole myöskään testannut joka-tunti-cron-job, mutta 2 minuutin sykleissä se toimi ainakin ihan ok.)