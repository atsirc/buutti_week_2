const fs = require('fs');
const fileName = 'logs/logs.json';

const logCommands = (obj) => {
    try {
        const data = fs.readFileSync(fileName , 'utf8');
        const logs = JSON.parse(data); 
        logs.push(obj);
        fs.writeFile(fileName, JSON.stringify(logs), 'utf8', (err) => {
            if (err) {
                console.log('Could not save logs to file!' );
            } else {
                console.log('Succeeded in saving file!');
            }
        });
    } catch (e) {
        if (e.message.includes('no such file')) {
            fs.writeFile(fileName, JSON.stringify([obj]), 'utf8', (err) => {
                if (err) {
                    console.log('Could not save logs to file!' );
                } 
            });
        }
    }
};

const archive = () => {
    try {
        const data = fs.readFileSync(fileName , 'utf8');
        const logs = JSON.parse(data); 
        const dateParts = new Date().toJSON().split(':');
        const archivedFileName= 'logs/log_' + dateParts[0] + dateParts[1] +'.json';
        fs.writeFile(archivedFileName, JSON.stringify(logs), 'utf8', (err) => {
            if (err) {
                console.log('Could not save logs to file!' );
            } else {
                console.log('Succeeded in saving file!');
            }
        });
        fs.unlinkSync(fileName);
    } catch(error) {
        console.log(error.message);
    }
};

module.exports = {
    logHandler: logCommands,
    archive: archive
};