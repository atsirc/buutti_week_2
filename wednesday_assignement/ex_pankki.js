const inquirer = require('inquirer');
const {logHandler} = require('./logger');
const giftAmount = 11;
const account = {
    balance: 120
};
const events = {
    'check_balance': 'Check balance',
    'deposit': 'Add some money',
    'withdraval':  `Donate ${giftAmount} € to charity`,
    'exit': 'Quit'
};

const handleChoice = async (answer, account) => {
    //Luo objektin joka lisätään logeihin. Ehkä olisi parmepaa että tätä luotaisiin logger.js:in sisällä...
    const logObj = {
        'timeStamp': Date.now(),
        'event': Object.keys(events).find(k => events[k] === answer.choice)
    };
    //Katsoo jos valinta on kaikki paitsi exit
    if (answer.choice === events.check_balance) {
        console.log(account.balance);
    } else if (answer.choice === events.deposit) {
        const result = await inquirer.prompt([{name: 'amount', message: 'Amount '}]);
        logObj.sum = result.amount;
        account.balance += Number(result.amount) || 0;
    } else if (answer.choice === events.withdraval) {
        logObj.sum = giftAmount;
        account.balance -= giftAmount;
    } 

    logHandler(logObj);
    //Vasta nyt katsoo jos exit, sillä halusin lähettää logObjektin ennen kun pankkitilin inquirer sammutetaan 
    if (answer.choice === events.exit) return 'exit';
    else return account;
};

const run = async (account) => {
    const choices = Object.values(events);
    while (true) {
        const answer = await inquirer.prompt([
            {
                type: 'list',
                name: 'choice',
                message:'Choose one of the following',
                choices: choices,
            },
        ]);
        const res = await handleChoice(answer, account);
        // res === 'exit'
        if (typeof res === 'string') return;
        account = {...res};
    }
};

run(account);