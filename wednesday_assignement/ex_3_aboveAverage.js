const process = require('process');
let arr = process.argv.slice(2);
arr = arr.length > 2 ? arr : [1,5,9,3];

const aboveAverage = arr => {
    const avg = arr.reduce((a,b) => a+b, 0) / arr.length;
    return arr.filter(val => val > avg);
};

console.log(aboveAverage(arr));