const process = require('process');
const num = typeof process.argv[2] !== 'undefined' ? Number(process.argv[2]) : 3;

//En muistanut, että ongelman nimi on Collatz conjecture, mutta oli kuitenkin muistikuvaa
//että on tunnettu matemaattinen ajatuspähkina -> funktion nimitys
const theProblem = (num, steps=0) => {
    if (num === 1) return steps;
    else {
        steps += 1;
        if (num % 2 === 0) {
            return theProblem(num/2, steps);
        } else  {
            return theProblem(num*3+1, steps);
        }
    }
};

//Huom, mielestäni vastaus esimerkkiin tulisi olla 7 eikä 6... Esim https://www.dcode.fr/collatz-conjecture
//mutta toki jos tekee jotain extra if lauseita pääsee siihen 6:een..
console.log(theProblem(num));