const prompt = require('prompt');
const correct_default = ['a', 'a', 'b', 'b'];
const exam_default =  ['a', 'c', 'b', 'd'];

const properties = [
    {
        description: 'Correct answers. Should be given with space as sepator',
        name: 'correctAnswers',
        validator: /\w+/,
        warning: 'exam answers should be given as a string with spaces as separators'
    },
    {
        description: 'Student\'s answers. Empty answers are marked \'\'',
        name: 'studentsAnswers',
        validator: /\w+/,
        warning: 'exam answers should be given as a string with spaces as separators empty answers are marked as empty strings i.e \'\''
    }
];

prompt.start();

prompt.get(properties, function (err, results) {
    if (err) {
        onErrorOrEmpty(err);
    }
    if (results.correctAnswers.length === 0) { 
        console.log(onErrorOrEmpty('empty string'));
    } else {
        const correct_answers = results.correctAnswers.split(' ');
        const student_answers = results.studentsAnswers.split(' ');
        console.log(checkAnswers(correct_answers, student_answers));
    }
});

const checkAnswers = (correct, exam) => {
    let sum = 0;
    exam.forEach((answer, idx) => {
        if (answer == correct[idx]) sum += 4;
        else if (!(answer == '\'\'' || answer == '')) sum -= 1;
    });
    return sum >= 0 ? sum : 0;
};

const onErrorOrEmpty = (message) => {
    console.log(message);
    return checkAnswers(correct_default, exam_default);
};