const process = require('process');
const word = process.argv[2] || 'abracadabra';

const countVowels = word => {
    const vowels = /[aeiouyAEIOUY]/g;
    return word.match(vowels).length;
};

console.log(countVowels(word));