const process = require('process');
let args = process.argv.slice(2);
args = args.length == 3 ? args : ['+', 2, 3];

const calculate = (operator, num1, num2) => {
    try {
        const result = eval(num1+operator+num2);
        console.log(result);
    } catch {
        console.log('Not a valid statement');
    }
};
calculate(...args);