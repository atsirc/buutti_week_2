const process = require('process');
const defaultNames = ['Alex', 'Linda', 'Mark', 'Max'];
const names = process.argv.slice(2);

const createString = names => {
    if (names.length < 2) {
        return `${names.length==0 ? 'No one' : names[0]} likes this.`;
    } else {
        //let persons = names.slice(0,2).join(', ');
        let persons = '';
        const [first, second, third] = names; 
        switch (names.length) {
           // case 2: persons = persons.replace(', ', ' and '); break;
           // case 3: persons = `${persons} and ${names[2]}`; break;
           // default: persons = `${persons} and ${names.length-2} others`;
           case 2: persons = `${first} and ${second}`;break;
           case 3: persons = `${first}, ${second} and ${third}`; break;
           default: persons = `${first}, ${second} and ${names.length - 2} others`;
        }
        return `${persons} like this`;
    }
};

console.log(createString(names));