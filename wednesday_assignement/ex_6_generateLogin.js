const process = require('process');
const _ = require('lodash');
const args = process.argv.slice(2,4);
const [name, surname] = args.length === 2 ? args : ['John', 'Doe'];

const randomChar = (start, end) => {
    return String.fromCharCode(_.random(start, end));
};

const generateCredentials = (name, surname) => {
    const [n1,n2] = [...name.toLowerCase()];
    const [s1,s2, ...rest] = [...surname.toLowerCase()];
    const currentYear = new Date().getFullYear() % 1000;
    const loginName = `B${currentYear}${s1}${s2}${n1}${n2}`;
    const password = `${randomChar(65, 90)}${n1}${rest.at(-1).toUpperCase()}${randomChar(33,47)}${currentYear}`;
    return {password: password, username: loginName};
};

console.log(generateCredentials(name, surname));