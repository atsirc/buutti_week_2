const {archive} = require('./logger');
const CronJob = require('cron').CronJob;
const job = new CronJob('0 0 */1 * * *', function() {
    console.log(new Date());
    console.log('Running chron job, result: ', archive());
}, null, true, 'Europe/Helsinki');
job.start();