const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];
const divisibleByThree = arr.filter(n => n%3==0)
console.log(divisibleByThree)
const doubledUp = arr.map(n=> n*2)
console.log(doubledUp)
const sumOfArray = arr.reduce((a,b) => a+b, 0);
console.log(sumOfArray)