const string = "aabbAAooooofffkkccdddTTTA"
// Alla oleva ei toimi jos sama kirjan (A) löytyy monesta paikasta 
// const result = [...string].find(c => string.match(new RegExp(c, 'g')).length===1)

const result = [...string].find((char,idx, arr) => {
     if (arr[idx+1] !== char && arr[idx-1] !== char) {
        return char
    }
})

console.log(result)
