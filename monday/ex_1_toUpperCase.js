const process = require('process');
const userInput = process.argv.slice(2).join(' ') || "This is a string";
const everyWordToUpper = (string) => {
    //const start = performance.now();
    const uppers = string.split(' ').map(n => n[0].toUpperCase() + n.substring(1));
    //const end = performance.now();
    //console.log(end-start)
    return uppers.join(' ');
}
console.log(everyWordToUpper(userInput));

const everyWordToUpperRegex = string => {
    return string.replace(/(^\w|\s\w)/g, hit => hit.toUpperCase())
}
console.log(everyWordToUpperRegex('testing a string that is different from the basic string'))