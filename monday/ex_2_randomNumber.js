const process = require('process');

const getRandom = (min, max) => {
    const realMax = Math.max(min, max);
    const realMin = Math.min(min, max);
    return Math.floor(Math.random() * (realMax - realMin) + realMin);
};
const min = Number(process.argv[2]) || 0;
const max = Number(process.argv[3]) + 1 || 3;
console.log(getRandom(min, max));