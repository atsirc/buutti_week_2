const getRandom = (min, max) => {
    return Math.floor(Math.random() * (max+1 - min) + min);
}
const numbers = []
while (numbers.length !== 7) {
    const newNum = getRandom(1,40)
    if (!numbers.includes(newNum)) {
        numbers.push(newNum)
    }
}

numbers.forEach((n,i) => setTimeout(()=> {console.log(n)}, i*1000))