const process = require('process');
const num = Number(process.argv[2]) || 5;
const getFactorial = num => {
    if (num !== 1) {
        return num * getFactorial(num-1);
    }
    return num;
}

console.log(getFactorial(num));