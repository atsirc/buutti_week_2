const fs = require('fs');
const process = require('process');

const file = fs.createReadStream('./ex_1_textfile.txt', 'utf-8');
//file.on('data', (txt)=> {
//    console.log('content of ex_1_textfile.txt');
//    console.log(txt);
//});

const writeStream = fs.createWriteStream('./ex_1_textfile2.txt');
file.on('data', (chunk) => { 
    let words = chunk.split(' ');
    //Huom, toisen rivin Joulun edessä on joku näkymätön newline (ei \n) jonka poistetaan tässä operaatiossa... 
    //Toisin voisihan käyttää replace sen sijaan että heitetään uusi sana...
    words = words.map(word => {
        if (word.toLowerCase().includes('joulu')) {
            word = word.includes('J') ? 'Kinkku' : 'kinkku';
        } else if (word.toLowerCase().includes('lapsi')) {
            word = word.includes('L') ? 'Poroilla' : 'poroilla';
        }
        process.stdout.write(word + ' ');
        return word;
    }); 

    writeStream.write(words.join(' '));
});