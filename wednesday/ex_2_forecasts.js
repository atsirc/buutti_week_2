const fs = require('fs');

const forecast = [{
    day: 'monday',
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
}];
const fileName = 'forecast_data.json';

const changeForecastsFromFile = (fileName) => {
    try {
        const data = fs.readFileSync(fileName , 'utf8');
        const allForecasts = JSON.parse(data); // allForecasts is some variable
        allForecasts.forEach(day => {
            if (day.day === 'monday') {
                day.temperature = '47';
            }
        });
        fs.writeFile(fileName, JSON.stringify(allForecasts), 'utf8', (err) => {
            if (err) {
                console.log('Could not save forecasts to file!' );
            } else {
                console.log('Succeeded in saving file!');
            }
        });
    } catch (e) {
        console.log('No saved data found.');
    }
};

fs.writeFile(fileName, JSON.stringify(forecast), 'utf8', (err) => {
    if (err) {
        console.log('Could not save forecasts to file!' );
    } else changeForecastsFromFile(fileName);
});