const fs = require('fs');
const forecast = {
    day: 'monday',
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};
const fileName = 'forecast_data.json';

const changeForecastsFromFile = (fileName) => {
    try {
        const data = fs.readFileSync(fileName , 'utf8');
        const allForecasts = JSON.parse(data); // allForecasts is some variable
        allForecasts.temperature = 90;
        saveForecastsToFile(allForecasts, fileName);
    } catch (e) {
        console.log('No saved data found.');
    }
};

const saveForecastsToFile = (obj, filename) => {
    fs.writeFile(filename, JSON.stringify(obj), 'utf8', (err) => {
        if (err) {
            console.log('Could not save forecasts to file!' );
        } else {
            console.log('Succeeded in writing file');
        }
    });
};

fs.writeFile(fileName, JSON.stringify(forecast), 'utf8', (err) => {
    if (err) {
        console.log('Could not save forecasts to file!' );
    }
    changeForecastsFromFile(fileName);
});