const process = require('process');
const string = process.argv.slice(2).join(' ') || 'This is a test string';
const words = string.split(' ').map(word => {
    const chars = [...word];
    return chars.reverse().join('');
});
console.log(words.join(' '));