const _ = require('lodash');
const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

const reorderArray = arr => {
    const maxIdx = array.length-1;
    const newArr = [];
    while (newArr.length != arr.length) {
        //const idx = Math.floor(Mah.random() * max);
        const idx = _.random(maxIdx);
        const element = arr[idx];
        if (!newArr.includes(element)) {
            newArr.push(element);
        }
    }
    return newArr;
};

console.log(reorderArray(array));