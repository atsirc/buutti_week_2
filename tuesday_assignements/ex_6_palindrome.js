const process = require('process');
const word = process.argv[2] || 'saippuakivikauppias';
const checkPalindrome = (word) => {
    const reversedWord = [...word].reverse().join('');
    if (word === reversedWord) {
        console.log(`Yes, ${word} is a palindrome`);
    } else {
        console.log(`No, ${word} is not a palindrome`);
    }
};
checkPalindrome(word);