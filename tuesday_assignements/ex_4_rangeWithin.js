const process = require('process');
const _ = require('lodash');
let start = Number(process.argv[2]) || 1;
let end = Number(process.argv[3]) || 10;
end += (start < end) ? 1 : -1;
console.log(_.range(start, end));