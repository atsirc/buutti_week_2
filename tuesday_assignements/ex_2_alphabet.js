/* HUOMIO. Tässä oli pakko käyttää ; koska muuten [...word] ei toiminut */
const process = require('process');
const word = process.argv[2] || 'bread';
const base = 'a'.charCodeAt(0) - 1;
[...word].forEach(char => process.stdout.write(`${char.charCodeAt(0)-base}`));
